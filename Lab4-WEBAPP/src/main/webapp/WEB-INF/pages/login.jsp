<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../jspf/header.jspf" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/security.js"></script>
<body>
<section class="vh-100 gradient-custom">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-12 col-md-8 col-lg-6 col-xl-5">
        <div class="card bg-dark text-white" style="border-radius: 1rem;">
          <div class="card-body p-5 text-center">

            <div class="mb-md-5 mt-md-4 pb-5">

              <h2 class="fw-bold mb-2 text-uppercase">Аутентификация</h2>
              <p class="text-white-50 mb-5">Пожалуйста, введите ваш логин и пароль</p>

              <div class="form-outline form-white mb-4">
                <input type="email" id="login" class="form-control form-control-lg" />
                <label class="form-label" for="login">Логин</label>
              </div>

              <div class="form-outline form-white mb-4">
                <input type="password" id="password" class="form-control form-control-lg" />
                <label class="form-label" for="password">Пароль</label>
              </div>

              <button id="login-but" class="btn btn-outline-light btn-lg px-5" type="submit" onclick="authenticate()">Войти</button>

              <div class="d-flex justify-content-center text-center mt-4 pt-1">
                <a href="#!" class="text-white"><i class="fab fa-facebook-f fa-lg"></i></a>
                <a href="#!" class="text-white"><i class="fab fa-twitter fa-lg mx-4 px-2"></i></a>
                <a href="#!" class="text-white"><i class="fab fa-google fa-lg"></i></a>
              </div>

            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</body>
<%@ include file="../jspf/footer.jspf" %>
