$(document).ready(function () {
    var exampleModal = document.getElementById('exampleModal')
    exampleModal.addEventListener('show.bs.modal', showCarCreationModal)
});

$(document).ready(function () {
    $("#savingCar").click(function () {
        var car = {
            id: $("#car-id").val(),
            carMark: $("#car-mark").val(),
            carNumber: $("#car-number").val(),
        };
        sendSavingCarRequest(car);
    });
});

function sendSavingCarRequest(car) {
    $.ajax({
        url: "cars",
        method: "POST",
        dataType: "text",
        data: JSON.stringify(car),
        success: function (data) {
            alert("Успешное создание записи");
            window.location.reload();
        },
        error: function (data) {
            console.log(data);
            alert("Произошла ошибка в ходе создания записи");
            window.location.reload();
        }
    });
}

function showCarCreationModal(event) {
    let inputs = $("#exampleModal").find(".modal-body input");
    if (event?.relatedTarget?.parentElement?.id) {
        $("#exampleModalLabel").text("Редактирование информации о машине в автопарке");
        $("#car-id").val(event.relatedTarget.parentElement.id);
        $("#car-mark").val($(event.relatedTarget.parentElement).find(".car-record-mark").text());
        $("#car-number").val($(event.relatedTarget.parentElement).find(".car-record-number").text());
    } else {
        $("#exampleModalLabel").text("Регистрация новой машины в автопарке");
        inputs.each(function () {
            this.value = "";
        });
    }

}

function sendDeletionRequest(carIdentifiers) {
    let request = {
        carIdentifiers: carIdentifiers
    };
    $.ajax({
        url: "cars",
        method: 'DELETE',
        dataType: "text",
        data: JSON.stringify(request),
        success: function (data) {
            alert("Успешное удаление записей");
            window.location.reload();
        },
        error: function (data) {
            alert("Произошла ошибка в ходе удаления записей");
        }
    });
}

function deleteCars() {
    var carIdentifiers = []
    for (const carRecord of document.querySelectorAll(".car-record")) {
        if (carRecord.querySelector("input[type=checkbox]").checked) {
            carIdentifiers.push(carRecord.id);
        }
    }
    sendDeletionRequest(carIdentifiers);
}


