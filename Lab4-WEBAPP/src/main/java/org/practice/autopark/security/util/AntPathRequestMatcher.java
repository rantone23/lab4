package org.practice.autopark.security.util;

import org.flywaydb.core.internal.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

public final class AntPathRequestMatcher implements RequestMatcher {
    private static final String MATCH_ALL = "/**";
    private final Matcher matcher;
    private final String pattern;
    private final HttpMethod httpMethod;

    public AntPathRequestMatcher(String pattern) {
        this(pattern, (String) null);
    }

    public AntPathRequestMatcher(String pattern, String httpMethod) {
        if (!pattern.equals("/**") && !pattern.equals("**")) {
            pattern = pattern.toLowerCase();
            if (pattern.endsWith("/**") && pattern.indexOf(63) == -1 && pattern.indexOf("*") == pattern.length() - 2) {
                this.matcher = new SubpathMatcher(pattern.substring(0, pattern.length() - 3));
            } else {
                this.matcher = new SpringAntMatcher(pattern);
            }
        } else {
            pattern = "/**";
            this.matcher = null;
        }

        this.pattern = pattern;
        this.httpMethod = StringUtils.hasText(httpMethod) ? HttpMethod.valueOf(httpMethod) : null;
    }

    public boolean matches(HttpServletRequest request) {
        if (this.httpMethod != null && this.httpMethod != HttpMethod.valueOf(request.getMethod())) {
            return false;
        } else if (this.pattern.equals("/**")) {
            return true;
        } else {
            String url = this.getRequestPath(request);
            return this.matcher.matches(url);
        }
    }

    private String getRequestPath(HttpServletRequest request) {
        String url = request.getServletPath();
        if (request.getPathInfo() != null) {
            url = url + request.getPathInfo();
        }

        url = url.toLowerCase();
        return url;
    }

    public String getPattern() {
        return this.pattern;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof AntPathRequestMatcher)) {
            return false;
        } else {
            AntPathRequestMatcher other = (AntPathRequestMatcher) obj;
            return this.pattern.equals(other.pattern) && this.httpMethod == other.httpMethod;
        }
    }

    public int hashCode() {
        int code = 31 ^ this.pattern.hashCode();
        if (this.httpMethod != null) {
            code ^= this.httpMethod.hashCode();
        }

        return code;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Ant [pattern='").append(this.pattern).append("'");
        if (this.httpMethod != null) {
            sb.append(", ").append(this.httpMethod);
        }

        sb.append("]");
        return sb.toString();
    }

    private static class SubpathMatcher implements Matcher {
        private final String subpath;
        private final int length;

        private SubpathMatcher(String subpath) {
            assert !subpath.contains("*");

            this.subpath = subpath;
            this.length = subpath.length();
        }

        public boolean matches(String path) {
            return path.startsWith(this.subpath) && (path.length() == this.length || path.charAt(this.length) == '/');
        }
    }

    private static class SpringAntMatcher implements Matcher {
        private static final AntPathMatcher antMatcher = new AntPathMatcher();
        private final String pattern;

        private SpringAntMatcher(String pattern) {
            this.pattern = pattern;
        }

        public boolean matches(String path) {
            return antMatcher.match(this.pattern, path);
        }
    }

    private interface Matcher {
        boolean matches(String var1);
    }
}
