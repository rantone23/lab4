package org.practice.autopark.security.filter;

import org.practice.autopark.security.authentication.AuthenticationManager;
import org.practice.autopark.security.util.AntPathRequestMatcher;

import javax.ejb.EJB;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@WebFilter({"*"})
public class AuthenticationFilter implements Filter {

    @EJB
    private AuthenticationManager authenticationManager;

    private List<AntPathRequestMatcher> allowedResources = new LinkedList<AntPathRequestMatcher>();

    public void init(FilterConfig filterConfig) throws ServletException {
        allowedResources.add(new AntPathRequestMatcher("/css/**"));
        allowedResources.add(new AntPathRequestMatcher("/js/**"));
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        if (httpServletRequest.getRequestURI().toUpperCase().endsWith("login".toUpperCase())
                || allowedResources.stream().anyMatch((e) -> e.matches(httpServletRequest))) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        boolean isAuthenticated = false;
        if (isAuthenticationRequired(httpServletRequest)) {
            String username = httpServletRequest.getParameter("j_username");
            String password = httpServletRequest.getParameter("j_password");
            isAuthenticated = authenticationManager.authenticate(username, password);
        } else if (authenticationManager.isAuthenticated()){
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        if (isAuthenticated)
            ((HttpServletResponse) servletResponse).sendRedirect(httpServletRequest.getContextPath());
        else ((HttpServletResponse) servletResponse).sendRedirect(httpServletRequest.getContextPath() + "/login");
    }

    protected boolean isAuthenticationRequired(HttpServletRequest httpServletRequest) {
        return httpServletRequest.getRequestURI().toUpperCase().endsWith("authenticate".toUpperCase())
                && httpServletRequest.getMethod().equalsIgnoreCase("POST");
    }

    public void destroy() {

    }
}
