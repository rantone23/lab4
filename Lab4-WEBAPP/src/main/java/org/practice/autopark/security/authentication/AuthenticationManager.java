package org.practice.autopark.security.authentication;

import org.practice.autopark.persistence.entity.UserEntity;
import org.practice.autopark.service.PasswordHashGenerator;
import org.practice.autopark.service.UserPolicy;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.xml.bind.DatatypeConverter;

@Singleton
public class AuthenticationManager {

    private boolean isAuthenticated = false;

    @EJB
    private PasswordHashGenerator passwordHashGenerator;

    @EJB
    private UserPolicy userPolicy;

    public boolean authenticate(String principal, String credentials) {
        byte[] hashedCredentials = passwordHashGenerator.generateHash(credentials);
        UserEntity user = userPolicy.findUser(principal, DatatypeConverter.printHexBinary(hashedCredentials));
        isAuthenticated = user != null;
        return isAuthenticated;
    }

    public boolean isAuthenticated() {
        return isAuthenticated;
    }
}
