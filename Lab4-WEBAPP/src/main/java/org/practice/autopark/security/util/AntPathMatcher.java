package org.practice.autopark.security.util;

import org.practice.autopark.util.StringUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AntPathMatcher implements PathMatcher {
    public static final String DEFAULT_PATH_SEPARATOR = "/";
    private static final Pattern VARIABLE_PATTERN = Pattern.compile("\\{[^/]+?\\}");
    private String pathSeparator = "/";
    private boolean trimTokens = true;
    private final Map<String, AntPathStringMatcher> stringMatcherCache = new ConcurrentHashMap(256);

    public AntPathMatcher() {
    }

    public void setPathSeparator(String pathSeparator) {
        this.pathSeparator = pathSeparator != null ? pathSeparator : "/";
    }

    public void setTrimTokens(boolean trimTokens) {
        this.trimTokens = trimTokens;
    }

    public boolean isPattern(String path) {
        return path.indexOf(42) != -1 || path.indexOf(63) != -1;
    }

    public boolean match(String pattern, String path) {
        return this.doMatch(pattern, path, true, (Map) null);
    }

    public boolean matchStart(String pattern, String path) {
        return this.doMatch(pattern, path, false, (Map) null);
    }

    protected boolean doMatch(String pattern, String path, boolean fullMatch, Map<String, String> uriTemplateVariables) {
        if (path.startsWith(this.pathSeparator) != pattern.startsWith(this.pathSeparator)) {
            return false;
        } else {
            String[] pattDirs = this.tokenizePath(pattern);
            String[] pathDirs = this.tokenizePath(path);
            int pattIdxStart = 0;
            int pattIdxEnd = pattDirs.length - 1;
            int pathIdxStart = 0;

            int pathIdxEnd;
            String pattDir;
            for (pathIdxEnd = pathDirs.length - 1; pattIdxStart <= pattIdxEnd && pathIdxStart <= pathIdxEnd; ++pathIdxStart) {
                pattDir = pattDirs[pattIdxStart];
                if ("**".equals(pattDir)) {
                    break;
                }

                if (!this.matchStrings(pattDir, pathDirs[pathIdxStart], uriTemplateVariables)) {
                    return false;
                }

                ++pattIdxStart;
            }

            int patIdxTmp;
            if (pathIdxStart > pathIdxEnd) {
                if (pattIdxStart > pattIdxEnd) {
                    return pattern.endsWith(this.pathSeparator) ? path.endsWith(this.pathSeparator) : !path.endsWith(this.pathSeparator);
                } else if (!fullMatch) {
                    return true;
                } else if (pattIdxStart == pattIdxEnd && pattDirs[pattIdxStart].equals("*") && path.endsWith(this.pathSeparator)) {
                    return true;
                } else {
                    for (patIdxTmp = pattIdxStart; patIdxTmp <= pattIdxEnd; ++patIdxTmp) {
                        if (!pattDirs[patIdxTmp].equals("**")) {
                            return false;
                        }
                    }

                    return true;
                }
            } else if (pattIdxStart > pattIdxEnd) {
                return false;
            } else if (!fullMatch && "**".equals(pattDirs[pattIdxStart])) {
                return true;
            } else {
                while (pattIdxStart <= pattIdxEnd && pathIdxStart <= pathIdxEnd) {
                    pattDir = pattDirs[pattIdxEnd];
                    if (pattDir.equals("**")) {
                        break;
                    }

                    if (!this.matchStrings(pattDir, pathDirs[pathIdxEnd], uriTemplateVariables)) {
                        return false;
                    }

                    --pattIdxEnd;
                    --pathIdxEnd;
                }

                if (pathIdxStart > pathIdxEnd) {
                    for (patIdxTmp = pattIdxStart; patIdxTmp <= pattIdxEnd; ++patIdxTmp) {
                        if (!pattDirs[patIdxTmp].equals("**")) {
                            return false;
                        }
                    }

                    return true;
                } else {
                    while (pattIdxStart != pattIdxEnd && pathIdxStart <= pathIdxEnd) {
                        patIdxTmp = -1;

                        int patLength;
                        for (patLength = pattIdxStart + 1; patLength <= pattIdxEnd; ++patLength) {
                            if (pattDirs[patLength].equals("**")) {
                                patIdxTmp = patLength;
                                break;
                            }
                        }

                        if (patIdxTmp == pattIdxStart + 1) {
                            ++pattIdxStart;
                        } else {
                            patLength = patIdxTmp - pattIdxStart - 1;
                            int strLength = pathIdxEnd - pathIdxStart + 1;
                            int foundIdx = -1;
                            int i = 0;

                            label140:
                            while (i <= strLength - patLength) {
                                for (int j = 0; j < patLength; ++j) {
                                    String subPat = pattDirs[pattIdxStart + j + 1];
                                    String subStr = pathDirs[pathIdxStart + i + j];
                                    if (!this.matchStrings(subPat, subStr, uriTemplateVariables)) {
                                        ++i;
                                        continue label140;
                                    }
                                }

                                foundIdx = pathIdxStart + i;
                                break;
                            }

                            if (foundIdx == -1) {
                                return false;
                            }

                            pattIdxStart = patIdxTmp;
                            pathIdxStart = foundIdx + patLength;
                        }
                    }

                    for (patIdxTmp = pattIdxStart; patIdxTmp <= pattIdxEnd; ++patIdxTmp) {
                        if (!pattDirs[patIdxTmp].equals("**")) {
                            return false;
                        }
                    }

                    return true;
                }
            }
        }
    }

    protected String[] tokenizePath(String path) {
        return StringUtils.tokenizeToStringArray(path, this.pathSeparator, this.trimTokens, true);
    }

    private boolean matchStrings(String pattern, String str, Map<String, String> uriTemplateVariables) {
        AntPathStringMatcher matcher = (AntPathStringMatcher) this.stringMatcherCache.get(pattern);
        if (matcher == null) {
            matcher = new AntPathStringMatcher(pattern);
            this.stringMatcherCache.put(pattern, matcher);
        }

        return matcher.matchStrings(str, uriTemplateVariables);
    }

    public String extractPathWithinPattern(String pattern, String path) {
        String[] patternParts = StringUtils.tokenizeToStringArray(pattern, this.pathSeparator, this.trimTokens, true);
        String[] pathParts = StringUtils.tokenizeToStringArray(path, this.pathSeparator, this.trimTokens, true);
        StringBuilder builder = new StringBuilder();
        int puts = 0;

        int i;
        for (i = 0; i < patternParts.length; ++i) {
            String patternPart = patternParts[i];
            if ((patternPart.indexOf(42) > -1 || patternPart.indexOf(63) > -1) && pathParts.length >= i + 1) {
                if (puts > 0 || i == 0 && !pattern.startsWith(this.pathSeparator)) {
                    builder.append(this.pathSeparator);
                }

                builder.append(pathParts[i]);
                ++puts;
            }
        }

        for (i = patternParts.length; i < pathParts.length; ++i) {
            if (puts > 0 || i > 0) {
                builder.append(this.pathSeparator);
            }

            builder.append(pathParts[i]);
        }

        return builder.toString();
    }

    public Map<String, String> extractUriTemplateVariables(String pattern, String path) {
        Map<String, String> variables = new LinkedHashMap();
        boolean result = this.doMatch(pattern, path, true, variables);
        return variables;
    }

    public String combine(String pattern1, String pattern2) {
        if (!StringUtils.hasText(pattern1) && !StringUtils.hasText(pattern2)) {
            return "";
        } else if (!StringUtils.hasText(pattern1)) {
            return pattern2;
        } else if (!StringUtils.hasText(pattern2)) {
            return pattern1;
        } else {
            boolean pattern1ContainsUriVar = pattern1.indexOf(123) != -1;
            if (!pattern1.equals(pattern2) && !pattern1ContainsUriVar && this.match(pattern1, pattern2)) {
                return pattern2;
            } else if (pattern1.endsWith("/*")) {
                return pattern2.startsWith("/") ? pattern1.substring(0, pattern1.length() - 1) + pattern2.substring(1) : pattern1.substring(0, pattern1.length() - 1) + pattern2;
            } else if (pattern1.endsWith("/**")) {
                return pattern2.startsWith("/") ? pattern1 + pattern2 : pattern1 + "/" + pattern2;
            } else {
                int dotPos1 = pattern1.indexOf(46);
                if (dotPos1 != -1 && !pattern1ContainsUriVar) {
                    String fileName1 = pattern1.substring(0, dotPos1);
                    String extension1 = pattern1.substring(dotPos1);
                    int dotPos2 = pattern2.indexOf(46);
                    String fileName2;
                    String extension2;
                    if (dotPos2 != -1) {
                        fileName2 = pattern2.substring(0, dotPos2);
                        extension2 = pattern2.substring(dotPos2);
                    } else {
                        fileName2 = pattern2;
                        extension2 = "";
                    }

                    String fileName = fileName1.endsWith("*") ? fileName2 : fileName1;
                    String extension = extension1.startsWith("*") ? extension2 : extension1;
                    return fileName + extension;
                } else {
                    return !pattern1.endsWith("/") && !pattern2.startsWith("/") ? pattern1 + "/" + pattern2 : pattern1 + pattern2;
                }
            }
        }
    }

    public Comparator<String> getPatternComparator(String path) {
        return new AntPatternComparator(path);
    }

    private static class AntPathStringMatcher {
        private static final Pattern GLOB_PATTERN = Pattern.compile("\\?|\\*|\\{((?:\\{[^/]+?\\}|[^/{}]|\\\\[{}])+?)\\}");
        private static final String DEFAULT_VARIABLE_PATTERN = "(.*)";
        private final Pattern pattern;
        private final List<String> variableNames = new LinkedList();

        public AntPathStringMatcher(String pattern) {
            StringBuilder patternBuilder = new StringBuilder();
            Matcher m = GLOB_PATTERN.matcher(pattern);

            int end;
            for (end = 0; m.find(); end = m.end()) {
                patternBuilder.append(this.quote(pattern, end, m.start()));
                String match = m.group();
                if ("?".equals(match)) {
                    patternBuilder.append('.');
                } else if ("*".equals(match)) {
                    patternBuilder.append(".*");
                } else if (match.startsWith("{") && match.endsWith("}")) {
                    int colonIdx = match.indexOf(58);
                    if (colonIdx == -1) {
                        patternBuilder.append("(.*)");
                        this.variableNames.add(m.group(1));
                    } else {
                        String variablePattern = match.substring(colonIdx + 1, match.length() - 1);
                        patternBuilder.append('(');
                        patternBuilder.append(variablePattern);
                        patternBuilder.append(')');
                        String variableName = match.substring(1, colonIdx);
                        this.variableNames.add(variableName);
                    }
                }
            }

            patternBuilder.append(this.quote(pattern, end, pattern.length()));
            this.pattern = Pattern.compile(patternBuilder.toString());
        }

        private String quote(String s, int start, int end) {
            return start == end ? "" : Pattern.quote(s.substring(start, end));
        }

        public boolean matchStrings(String str, Map<String, String> uriTemplateVariables) {
            Matcher matcher = this.pattern.matcher(str);
            if (!matcher.matches()) {
                return false;
            } else {
                if (uriTemplateVariables != null) {
                    for (int i = 1; i <= matcher.groupCount(); ++i) {
                        String name = (String) this.variableNames.get(i - 1);
                        String value = matcher.group(i);
                        uriTemplateVariables.put(name, value);
                    }
                }

                return true;
            }
        }
    }

    private static class AntPatternComparator implements Comparator<String> {
        private final String path;

        private AntPatternComparator(String path) {
            this.path = path;
        }

        public int compare(String pattern1, String pattern2) {
            if (pattern1 == null && pattern2 == null) {
                return 0;
            } else if (pattern1 == null) {
                return 1;
            } else if (pattern2 == null) {
                return -1;
            } else {
                boolean pattern1EqualsPath = pattern1.equals(this.path);
                boolean pattern2EqualsPath = pattern2.equals(this.path);
                if (pattern1EqualsPath && pattern2EqualsPath) {
                    return 0;
                } else if (pattern1EqualsPath) {
                    return -1;
                } else if (pattern2EqualsPath) {
                    return 1;
                } else {
                    int wildCardCount1 = this.getWildCardCount(pattern1);
                    int wildCardCount2 = this.getWildCardCount(pattern2);
                    int bracketCount1 = StringUtils.countOccurrencesOf(pattern1, "{");
                    int bracketCount2 = StringUtils.countOccurrencesOf(pattern2, "{");
                    int totalCount1 = wildCardCount1 + bracketCount1;
                    int totalCount2 = wildCardCount2 + bracketCount2;
                    if (totalCount1 != totalCount2) {
                        return totalCount1 - totalCount2;
                    } else {
                        int pattern1Length = this.getPatternLength(pattern1);
                        int pattern2Length = this.getPatternLength(pattern2);
                        if (pattern1Length != pattern2Length) {
                            return pattern2Length - pattern1Length;
                        } else if (wildCardCount1 < wildCardCount2) {
                            return -1;
                        } else if (wildCardCount2 < wildCardCount1) {
                            return 1;
                        } else if (bracketCount1 < bracketCount2) {
                            return -1;
                        } else {
                            return bracketCount2 < bracketCount1 ? 1 : 0;
                        }
                    }
                }
            }
        }

        private int getWildCardCount(String pattern) {
            if (pattern.endsWith(".*")) {
                pattern = pattern.substring(0, pattern.length() - 2);
            }

            return StringUtils.countOccurrencesOf(pattern, "*");
        }

        private int getPatternLength(String pattern) {
            Matcher m = AntPathMatcher.VARIABLE_PATTERN.matcher(pattern);
            return m.replaceAll("#").length();
        }
    }
}
