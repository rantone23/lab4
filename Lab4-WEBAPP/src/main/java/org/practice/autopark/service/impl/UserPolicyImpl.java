package org.practice.autopark.service.impl;

import org.practice.autopark.persistence.entity.UserEntity;
import org.practice.autopark.service.UserPolicy;

import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Singleton
@Local(UserPolicy.class)
public class UserPolicyImpl implements UserPolicy {

    @PersistenceContext(unitName = "AutoPark")
    private EntityManager entityManager;


    public UserEntity findUser(String principal, String credentials) {
        List<UserEntity> result = entityManager.createNamedQuery("User.findByUsernameAndPassword", UserEntity.class)
                .setParameter("username", principal)
                .setParameter("password", credentials)
                .getResultList();
        return result != null && !result.isEmpty() ? result.get(0) : null;
    }
}
