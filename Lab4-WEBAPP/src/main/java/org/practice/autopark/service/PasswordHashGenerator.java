package org.practice.autopark.service;

public interface PasswordHashGenerator {

    byte[] generateHash(String password);
}
