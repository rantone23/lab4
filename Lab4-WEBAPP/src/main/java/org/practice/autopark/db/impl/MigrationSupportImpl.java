package org.practice.autopark.db.impl;

import org.flywaydb.core.Flyway;
import org.practice.autopark.db.MigrationSupport;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.*;
import javax.sql.DataSource;

@Local(MigrationSupport.class)
@Singleton
@Startup
@TransactionManagement(value = TransactionManagementType.BEAN)
public class MigrationSupportImpl implements MigrationSupport {

    public static final String DB_MIGRATION_PATH = "db/migration";
    @Resource(lookup = "java:/AutoPark")
    private DataSource dataSource;

    @PostConstruct
    public void migrate() {
        Flyway flyway = Flyway
                .configure()
                .dataSource(dataSource)
                .group(false)
                .locations(DB_MIGRATION_PATH)
                .load();
        flyway.migrate();
    }
}
