package org.practice.autopark.persistence.entity;

import javax.persistence.*;

@Entity
@Table(name = "car")
@NamedQueries( {
        @NamedQuery(
                name = "Car.findAll",
                query = "select object(o) from CarEntity o"
        ),
        @NamedQuery(
                name = "Car.deleteByIdentifiers",
                query = "delete from CarEntity o where o.id in :identifiers"
        )
}
)
public class CarEntity {
    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "identifier", sequenceName = "car_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "identifier")
    private Long id;

    @Column(name = "mark")
    private String mark;

    @Column(name = "number")
    private String number;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMark() {
        return this.mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
